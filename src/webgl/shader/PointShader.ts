import Shader from './Shader';
import { POINT_FRAGMENT, POINT_VERTEX } from '../GLSL/point/shader';

/**
 *
 * @class GridShader
 * @extends {Shader}
 */
class PointShader extends Shader {
	constructor(gl: WebGL2RenderingContext, pMatrix: any) {
		super(gl, POINT_VERTEX, POINT_FRAGMENT);

		// Standard uniforms, we have to always activate our program
		this.setPerspectiveMatrix(pMatrix);

		// Location of custom uniforms in shader program
		this.uniformLocation.time = gl.getUniformLocation(this.program, 'time');
		this.uniformLocation.functionType = gl.getUniformLocation(this.program, 'functionType');

		// finish setting up shader
		gl.useProgram(null);
	}

	public setTime(): PointShader {
		this.gl.uniform1f(this.uniformLocation.time, performance.now());
		return this;
	}

	public setFunction(functionType: string): PointShader {
		this.gl.uniform1i(this.uniformLocation.functionType, parseInt(functionType, 10));
		return this;
	}
}

export default PointShader;
