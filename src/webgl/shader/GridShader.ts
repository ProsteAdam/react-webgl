import Shader from './Shader';
import { GRID_FRAGMENT, GRID_VERTEX } from '../GLSL/grid/shader';

/**
 *
 * @class GridShader
 * @extends {Shader}
 */
export class GridShader extends Shader {
	constructor(gl: WebGL2RenderingContext, pMatrix: any) {
		super(gl, GRID_VERTEX, GRID_FRAGMENT);

		// Standard uniforms, we have to always activate our program

		this.types = [0.0, 1.0];

		this.setPerspectiveMatrix(pMatrix);

		// Location of custom uniforms in shader program
		this.uniformLocation.time = gl.getUniformLocation(this.program, 'time');

		// Finish setting up shader
		gl.useProgram(null);
	}

	public setTime(): GridShader {
		this.gl.uniform1f(this.uniformLocation.time, performance.now());
		return this;
	}
}

export default GridShader;
