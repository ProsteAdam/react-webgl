import Shader from './Shader';
import Model from '../Model';
import { SPHERE_FRAGMENT, SPHERE_VERTEX } from '../GLSL/sphere/shader';

/**
 *
 * @class SphereShader
 * @extends {Shader}
 */
class SphereShader extends Shader {
	private time: number;
	private rotationPosition: number;

	constructor(gl: WebGL2RenderingContext, pMatrix: any) {
		super(gl, SPHERE_VERTEX, SPHERE_FRAGMENT);

		this.rotationPosition = 0;
		this.time = 0;

		// Custom uniforms
		this.uniformLocation.frequency = gl.getUniformLocation(this.program, 'uSoundFreq');

		this.setPerspectiveMatrix(pMatrix);

		// Finish setting up shader
		gl.useProgram(null);
	}

	private incrementPosition() {
		let position = this.rotationPosition + 0.05;
		if (this.rotationPosition > 360) {
			position = 0;
		}
		this.rotationPosition = position;
		return this;
	}

	public renderPositionedModel(model: Model): SphereShader {
		const position = this.rotationPosition;
		this.incrementPosition();
		const x = 10 * Math.cos(position);
		const z = 10 * Math.sin(position);
		this.renderModel(model.setPosition(x, 1, z).preRender());
		return this;
	}
}

export default SphereShader;
