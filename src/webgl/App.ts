import Camera from './Camera';
import GL from './GL';
import CameraController from './CameraController';
import Mesh from './Mesh';
import { Resources, RenderLoop } from './utils';
import { CubeShader, GridShader, PointShader, SkyMapShader, SphereShader } from './shader';

/**
 * Application class
 *
 * @class App
 */
export class App {
	public webGL: WebGL2RenderingContext;
	public GL: GL;
	public mesh: Mesh;
	public resources: Resources;

	public camera: Camera;
	public cameraController: CameraController;

	public shaderCache: unknown[];
	public modelCache: unknown[];

	public loop: RenderLoop;

	constructor(public sizeOfGrid: number) {
		this.GL = new GL('canvas').fitScreen().clear();
		this.webGL = this.GL.getGL();
		this.mesh = new Mesh(this.webGL);
		this.resources = new Resources(this.webGL);

		this.camera = new Camera(this.webGL);
		this.cameraController = new CameraController(this.webGL, this.camera);

		this.loop = new RenderLoop();

		this.modelCache = [];
		this.shaderCache = [];

		this.initialize();
	}

	public initialize = (): App => {
		return this.initCamera()
			.loadResources()
			.loadShaders()
			.loadModels();
	};

	public start(onRender: (deltaTime: number, fps: number) => void, fps: number = 60): RenderLoop {
		return this.loop
			.setCallback(onRender)
			.setFPS(fps)
			.start();
	}

	public initCamera(): App {
		// Setting our initial position
		this.camera.transform.reset().setPosition(1, 0, 15);
		return this;
	}

	public loadResources(): App {
		// Load sky box texture
		this.resources.loadCubeMap('skybox', [
			document.getElementById('cube_right'),
			document.getElementById('cube_left'),
			document.getElementById('cube_top'),
			document.getElementById('cube_bottom'),
			document.getElementById('cube_back'),
			document.getElementById('cube_front'),
		]);
		return this;
	}

	public loadShaders(): App {
		this.shaderCache['GridShader'] = new GridShader(this.webGL, this.camera.projectionMatrix);

		this.shaderCache['PointShader'] = new PointShader(this.webGL, this.camera.projectionMatrix);

		this.shaderCache['CubeShader'] = new CubeShader(this.webGL, this.camera.projectionMatrix);

		this.shaderCache['SphereShader'] = new SphereShader(this.webGL, this.camera.projectionMatrix);

		this.shaderCache['SkyMapShader'] = new SkyMapShader(
			this.webGL,
			this.camera.projectionMatrix,
			this.resources.getTexture('skybox')
		);

		return this;
	}

	public loadModels(): App {
		this.modelCache['GridModel'] = this.mesh.createGrid();

		this.modelCache['CubeModel'] = this.mesh.createCube();

		this.modelCache['CubeModel'].setPosition(0, 1, 0).setScale(1, 2, 1);

		this.modelCache['PointModel'] = this.mesh.createPoints();

		this.modelCache['SphereModel'] = this.mesh.createSphere();

		this.modelCache['SkyboxModel'] = this.mesh.createSkyBox('Skybox', 50, 50, 50, 0, 0, 0);

		return this;
	}

	public setSizeOfGrid(value: number): App {
		this.sizeOfGrid = value;
		this.modelCache['GridModel'] = this.mesh.createGrid(Math.floor(Math.sqrt(value)));
		this.modelCache['PointModel'] = this.mesh.createPoints(Math.floor(Math.sqrt(value)));
		return this;
	}

	public setCameraType(cameraType: 'ORTHOGONAL' | 'PERSPECTIVE'): App {
		this.camera.setCameraType(cameraType);
		return this.initialize();
	}

}

export default App;
