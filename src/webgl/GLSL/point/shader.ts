import { HSL_TO_RGB } from '../utility/color';

export const POINT_VERTEX: string = `#version 300 es
layout(location=0) in vec3 a_position; // Standard position data

uniform mat4 uMVMatrix;
uniform mat4 uPMatrix;
uniform mat4 uCameraMatrix;

uniform float time;
uniform float type;

uniform int functionType;

${HSL_TO_RGB}

float waveFunction(in vec2 pos, float time) {
		float offset = time * 0.0005;
    return 0.2 * sin((pos.x + offset) * 5.0) * cos((pos.y + offset) * 5.0);
}

float explicitFunction(in vec2 pos){
	float distance = sqrt(pos.x * pos.x + pos.y * pos.y);
	return cos(distance * 3.14 * 2.0);
}

out vec4 color;

void main(void){

		vec2 positionXZ = vec2(a_position.xz);
		float resultZ = waveFunction(positionXZ, time);
		
		switch(functionType) {
			case 1: 
				resultZ = waveFunction(positionXZ, time);
				break;
			case 2:
				resultZ = explicitFunction(positionXZ);
				break;	
			default:
				break;	
		}
		
    vec3 P = vec3(a_position.x, resultZ, a_position.z);

    vec3 huecolor = hsl2rgb(P.y);
    color = vec4(huecolor, 1.0);
    
    gl_PointSize = 2.0;
    gl_Position = uPMatrix * uCameraMatrix * uMVMatrix * vec4(P, 1.0);
}
`;

export const POINT_FRAGMENT: string = `#version 300 es
precision mediump float;

in vec4 color;

out vec4 finalColor;

void main(void) {
    finalColor = color;
}
`;
