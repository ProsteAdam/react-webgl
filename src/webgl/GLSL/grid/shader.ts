import { HSL_TO_RGB } from '../utility/color';

export const GRID_VERTEX: string = `#version 300 es
layout(location=0) in vec3 a_position; // Standard position data

uniform mat4 uMVMatrix;
uniform mat4 uPMatrix;
uniform mat4 uCameraMatrix;

uniform float time;
uniform float type;

out vec3 vertColor;
out vec3 normal;
out vec3 light;

out vec3 viewDirection;

float getWave(float position) {
	return sin(position * 3.14 * 2.0);
}

float waveFunction(in vec2 pos) {
		float offset = time * 0.0005;
    return 0.2 * sin((pos.x + offset) * 5.0) * cos((pos.y + offset) * 5.0);
}

vec3 getSphere(vec2 vec){
	float az = vec.x * 3.14;
	float ze = vec.y * 3.14 / 2.0;
	float r = 1.0;

	float x = r * cos(az) * cos(ze);
	float y = 2.0 * r * sin(az) * cos(ze);
	float z = 0.5 * r * sin(ze);
	return vec3(x, y, z);
}

vec3 getSphereNormal(vec2 vec){
	vec3 u = getSphere(vec + vec2(0.001, 0)) - getSphere(vec - vec2(0.001, 0));
	vec3 v = getSphere(vec + vec2(0, 0.001)) - getSphere(vec - vec2(0, 0.001));
	return cross(u,v);
}

vec3 getPlane(vec2 vec){
	return vec3(vec * 4.0, -2.0);
}

vec3 getPlaneNormal(vec2 vec){
	vec3 u = getPlane(vec + vec2(0.001, 0)) - getPlane(vec - vec2(0.001, 0));
	vec3 v = getPlane(vec + vec2(0, 0.001)) - getPlane(vec - vec2(0, 0.001));
	return cross(u,v);
}

${HSL_TO_RGB}

void main(void){

	mat4 view = uCameraMatrix * uMVMatrix;
	mat4 projection = uPMatrix;

  vec2 pos;
  vec4 pos4;
  	
  // we go from -1 to 1
	pos = (a_position * 2.0 - 1.0).xz;
	
	vec2 positionXZ = vec2(a_position.xz);
	
	float resultZ = waveFunction(positionXZ);
	
	if(type == 1.0){
		pos4 = vec4(getPlane(pos), 1.0);
		normal = mat3(view) * getPlaneNormal(pos);
	}	else	{
		pos4 = vec4(getSphere(pos), 1.0);
		normal = mat3(view) * getSphereNormal(pos);
	}
	
	gl_Position = projection * view * pos4;

	vec3 lightPos = vec3(1.0, 1.0, 0);
	
	light = lightPos - (view * pos4).xyz;

	viewDirection = -pos4.xyz;
  
}
`;

export const GRID_FRAGMENT: string = `#version 300 es
precision mediump float;

out vec4 outColor;

in vec3 vertColor;
in vec3 normal;
in vec3 light;
in vec3 viewDirection;

void main(void) {
	vec4 ambient = vec4(vec3(0.25), 1.0);
	
	float NdotL = dot(normalize(normal), normalize(light));
	
	vec4 diffuse = vec4(NdotL * vec3(0.35), 1.0);
	
	vec3 halfVector = normalize(light) + normalize(viewDirection);
	
	float NdotH = dot(normalize(normal), halfVector);
	
	vec4 specular = vec4(pow(NdotH, 16.0) * vec3(0.25), 1.0);
	
	outColor = ambient + diffuse + specular;
}
`;
