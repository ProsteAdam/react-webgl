import { mat4, glMatrix } from 'gl-matrix';
import Transform from './Transformer';
import { DEG2RAD } from './utils/constants';

/**
 * Camera representation
 *
 * @class Camera
 */
class Camera {
	static cameraType = {
		ORTHOGONAL: 'ORTHOGONAL',
		PERSPECTIVE: 'PERSPECTIVE',
	};

	public projectionMatrix: mat4;
	public transform: Transform;
	public viewMatrix: mat4;

	private readonly ratio: number;

	private cameraType: string = Camera.cameraType.PERSPECTIVE;

	constructor(
		private gl: WebGL2RenderingContext,
		private fov: number = 45,
		private near: number = 0.01,
		private far: number = 100.0
	) {
		this.ratio = gl.canvas.width / gl.canvas.height;

		this.initializeCamera();
	}

	private initializeCamera = (): Camera => {
		// Our projection matrix is held in camera
		this.projectionMatrix = mat4.create();
		if (this.cameraType === Camera.cameraType.PERSPECTIVE) {
			mat4.perspective(this.projectionMatrix, glMatrix.toRadian(this.fov as number), this.ratio, this.near, this.far);
		} else {
			mat4.ortho(this.projectionMatrix, -1.0, 1.0, -1.0, 1.0, this.near, this.far);
		}

		this.transform = new Transform(); // Initialize new transform class to control the position of the camera
		this.viewMatrix = mat4.create(); // Hold our camera matrix
		return this;
	};

	// We are simply adding calculated number to our transformation position vector
	public moveX(x): void {
		this.updateViewMatrix();
		this.transform.position[0] += this.transform.right[0] * x;
		this.transform.position[1] += this.transform.right[1] * x;
		this.transform.position[2] += this.transform.right[2] * x;
	}

	public moveY(y): void {
		this.updateViewMatrix();
		this.transform.position[0] += this.transform.up[0] * y;
		this.transform.position[1] += this.transform.up[1] * y;
		this.transform.position[2] += this.transform.up[2] * y;
	}

	// We are moving in the direction we are looking at
	public moveZ(z): void {
		this.updateViewMatrix();
		this.transform.position[0] += this.transform.forward[0] * z;
		this.transform.position[1] += this.transform.forward[1] * z;
		this.transform.position[2] += this.transform.forward[2] * z;
	}

	// Updating our camera matrice, with given transformation vectors from Transform class
	public updateViewMatrix(): mat4 {
		// Make it identity matrix
		// First translate then rotate
		this.transform.resetModelMatrix();
		mat4.translate(this.transform.matView, this.transform.matView, this.transform.position);
		mat4.rotateX(this.transform.matView, this.transform.matView, this.transform.rotation[0] * DEG2RAD);
		mat4.rotateY(this.transform.matView, this.transform.matView, this.transform.rotation[1] * DEG2RAD);

		this.transform.updateDirection();

		// Doing inverse transformation on all meshes
		mat4.invert(this.viewMatrix, this.transform.matView);

		return this.viewMatrix;
	}

	// Using this on SkyMap Shader so the sky wont move, we do it by translating the position in matrix always to zero
	public getTranslatelessMatrix(): Float32Array {
		let mat = new Float32Array(this.viewMatrix);
		// Reset Translation position in the matrix to zero
		mat[12] = mat[13] = mat[14] = 0.0;
		return mat;
	}

	public setCameraType = (cameraType: 'ORTHOGONAL' | 'PERSPECTIVE'): Camera => {
		this.cameraType = cameraType;
		return this.initializeCamera();
	};
}

export default Camera;
