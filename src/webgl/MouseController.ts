import Model from './Model';
import { RIGHT_MOUSE_BUTTON } from './utils/constants';

/**
 * Simple mouse controller class
 *
 * @class MouseController
 */
class MouseController {
	public canvas: HTMLCanvasElement;

	public initX: number;
	public initY: number;

	public prevX: number;
	public prevY: number;

	public rotateRate: number;
	public panRate: number;
	public zoomRate: number;

	public offsetX: number;
	public offsetY: number;

	public onUpHandler = (e: MouseEvent) => {};
	public onMoveHandler = (e: MouseEvent) => {};

	constructor(private gl: WebGL2RenderingContext | any, private model: Model) {
		let box: ClientRect = gl.canvas.getBoundingClientRect();

		// For sake of brevity for comment lookup visit CameraController.ts file since this is just copy paste from there
		this.canvas = gl.canvas;

		this.offsetX = box.left;
		this.offsetY = box.top;

		this.rotateRate = -300;
		this.panRate = 30;
		this.zoomRate = 300;

		this.prevX = 0;
		this.prevY = 0;
		this.initX = 0;
		this.initY = 0;

		this.onUpHandler = (e) => {
			this.onMouseUp(e);
		};
		this.onMoveHandler = (e) => {
			this.onMouseMove(e);
		};

		this.canvas.addEventListener('mousedown', (e: MouseEvent) => {
			if (e.button === RIGHT_MOUSE_BUTTON) {
				this.onMouseDown(e);
			}
		});
	}

	// Dragging movement listener
	public onMouseDown(e: MouseEvent): void {
		this.initX = this.prevX = e.pageX - this.offsetX;
		this.initY = this.prevY = e.pageY - this.offsetY;

		this.canvas.addEventListener('mouseup', this.onUpHandler);
		this.canvas.addEventListener('mousemove', this.onMoveHandler);
	}

	// Remove dragging listener
	public onMouseUp(e: MouseEvent): void {
		this.canvas.removeEventListener('mouseup', this.onUpHandler);
		this.canvas.removeEventListener('mousemove', this.onMoveHandler);
	}

	public onMouseMove(e: MouseEvent): void {
		// Get x,y position
		const x: number = e.pageX - this.offsetX,
			y: number = e.pageY - this.offsetY,
			// position deltas
			dx: number = this.prevX - x,
			dy: number = this.prevY - y;

		this.model.addRotation(dy * (this.rotateRate / this.canvas.width), dx * (this.rotateRate / this.canvas.width), 0);

		this.prevX = x;
		this.prevY = y;
	}
}

export default MouseController;
