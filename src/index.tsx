import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App/App';

import './components/default.css';

ReactDOM.render(<App />, document.getElementById('root'));
