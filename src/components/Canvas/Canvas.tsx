import React, { PureComponent } from 'react';
import { Grid, Box } from 'grommet';
import { throttle } from 'throttle-debounce';
import App from '../../webgl/App';
import Camera from '../../webgl/Camera';
import { CUBES, GRID, POINTS, SPHERE } from '../../webgl/utils/constants';
import SideControl from '../SideControl/SideControl';
import FunctionType from '../FunctionType/FunctionType';

class Canvas extends PureComponent {
	private app: App;

	state = {
		selected: GRID,
		gridSize: 4000,
		cameraType: Camera.cameraType.PERSPECTIVE,
		skybox: false,
		fps: 60,
		functionType: '1',
	};

	componentDidMount(): void {
		this.app = new App(this.state.gridSize);
		this.app.start(this.onRender);
	}

	onRender = (_, fps) => {
		this.handleThrottledSetFps(fps);
		const app: App = this.app;
		app.camera.updateViewMatrix();
		app.GL.clear();
		switch (this.state.selected) {
			case GRID: {
				app.shaderCache['GridShader']
					.activate()
					.setCameraMatrix(app.camera.viewMatrix)
					.setTime()
					.renderModel(app.modelCache['GridModel'].preRender())
					.deactivate();
				break;
			}
			case POINTS: {
				app.shaderCache['PointShader']
					.activate()
					.setCameraMatrix(app.camera.viewMatrix)
					.setTime()
					.setFunction(this.state.functionType)
					.renderModel(app.modelCache['PointModel'].preRender())
					.deactivate();
				break;
			}
			case CUBES: {
				app.shaderCache['CubeShader']
					.activate()
					.setCameraMatrix(app.camera.viewMatrix)
					.renderModel(app.modelCache['CubeModel'].preRender())
					.deactivate();
				break;
			}
			case SPHERE: {
				app.shaderCache['SphereShader']
					.activate()
					.setCameraMatrix(app.camera.viewMatrix)
					.renderPositionedModel(app.modelCache['SphereModel'])
					.deactivate();
				break;
			}
		}
		if (this.state.skybox) {
			// We must call setTexture method every time we use texture
			app.shaderCache['SkyMapShader']
				.activate()
				.setTexture()
				.setCameraMatrix(app.camera.getTranslatelessMatrix())
				.renderModel(app.modelCache['SkyboxModel'])
				.deactivate();
		}
	};

	handleThrottledSetFps = throttle(500, (fps) => this.setState({ fps }));

	handleCameraTypeUpdate = (cameraType) =>
		this.setState({ cameraType }, () => {
			this.app.setCameraType(cameraType);
		});

	handleGridSizeUpdate = (gridSize) =>
		this.setState({ gridSize }, () => {
			this.app.setSizeOfGrid(gridSize);
		});

	handleSetSelectedShape = (selected) => this.setState({ selected });

	handleSkyboxCheck = (skybox) => this.setState({ skybox });

	handleSetSelectedFunctionType = (functionType) => this.setState({ functionType });

	render() {
		const { selected, cameraType, gridSize, skybox, fps, functionType } = this.state;
		return (
			<Grid
				fill
				areas={[{ name: 'nav', start: [0, 0], end: [0, 0] }, { name: 'main', start: [1, 0], end: [1, 0] }]}
				columns={['small', 'flex']}
				rows={['flex']}
				gap="none"
			>
				<Box gridArea="nav" background="neutral-3">
					<SideControl
						cameraType={cameraType}
						handleCameraChange={this.handleCameraTypeUpdate}
						gridSize={gridSize}
						handleGridChange={this.handleGridSizeUpdate}
						shapeType={selected}
						handleShapeChange={this.handleSetSelectedShape}
						skybox={skybox}
						handleSkyboxChange={this.handleSkyboxCheck}
						fps={fps}
					>
						<FunctionType value={functionType} setValue={this.handleSetSelectedFunctionType} />
					</SideControl>
				</Box>
				<Box gridArea="main" background="accent-2" id="canvas-container">
					<canvas id="canvas" />
				</Box>
			</Grid>
		);
	}
}

export default Canvas;
