import React, { memo, ReactNode } from 'react';
import { Box, Clock } from 'grommet';

import GridSlider from '../GridSlider/GridSlider';
import FpsMeter from '../FpsMeter/FpsMeter';
import CameraType from '../CameraType/CameraType';
import ShapeType from '../ShapeType/ShapeType';
import Skybox from '../Skybox/Skybox';

type Props = {
	shapeType: string;
	cameraType: string;
	gridSize: number;
	handleGridChange: (value: number) => void;
	handleCameraChange: (value: string) => void;
	handleShapeChange: (value: string) => void;
	skybox: boolean;
	handleSkyboxChange: (value: boolean) => void;
	fps: number;
	children: ReactNode;
};

const SideControl = ({
	gridSize,
	skybox,
	handleSkyboxChange,
	handleGridChange,
	cameraType,
	handleCameraChange,
	shapeType,
	handleShapeChange,
	fps,
	children,
}: Props) => (
	<Box gridArea="nav" pad="medium">
		<Clock type="digital" alignSelf="center" />
		<FpsMeter value={fps} />
		<GridSlider value={gridSize} handleChange={handleGridChange} />
		<CameraType value={cameraType} setValue={handleCameraChange} />
		<ShapeType value={shapeType} setValue={handleShapeChange} />
		<Skybox value={skybox} handleChange={handleSkyboxChange} />
		{children}
	</Box>
);

export default memo(SideControl);
