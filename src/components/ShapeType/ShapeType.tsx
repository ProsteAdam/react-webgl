import React, { memo } from 'react';
import { RadioButtonGroup, Text } from 'grommet';
import { GRID, SPHERE, POINTS } from '../../webgl/utils/constants';

type Props = {
	value: string;
	setValue: (value: string) => void;
};

const ShapeType = ({ value, setValue }: Props) => (
	<>
		<Text>Shape</Text>
		<RadioButtonGroup
			pad="xsmall"
			name="radio"
			options={[
				{ label: 'Grid', value: GRID },
				// { label: 'Cubes', value: CUBES },
				{ label: 'Sphere', value: SPHERE },
				{ label: 'Points', value: POINTS },
			]}
			value={value}
			onChange={(event) => setValue(event.target.value)}
		/>
	</>
);

export default memo(ShapeType);
