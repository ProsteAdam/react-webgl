import React, { memo } from 'react';
import { RangeInput, FormField } from 'grommet';

import './gridSlider.css';

type Props = {
	value: number;
	handleChange: (value: number) => void;
};

const GridSlider = ({ value, handleChange }: Props) => (
	<FormField label={`Grid - ${value}`} htmlFor="grid" className="Slider">
		<RangeInput
			id="grid"
			min={0}
			max={20000}
			step={1}
			value={value}
			onChange={(event) => handleChange(Number(event.target.value))}
		/>
	</FormField>
);

export default memo(GridSlider);
