import React, { memo } from 'react';
import { Text, RadioButtonGroup } from 'grommet';
import Camera from '../../webgl/Camera';

type Props = {
	value: string;
	setValue: (value: string) => void;
};

const CameraType = ({ value, setValue }: Props) => (
	<>
		<Text>Camera</Text>
		<RadioButtonGroup
			pad="xsmall"
			name="radio"
			options={[
				{ label: 'Orthogonal', value: Camera.cameraType.ORTHOGONAL },
				{ label: 'Perspective', value: Camera.cameraType.PERSPECTIVE },
			]}
			value={value}
			onChange={(event) => setValue(event.target.value)}
		/>
	</>
);

export default memo(CameraType);
