import React, { memo } from 'react';
import { RadioButtonGroup, Text } from 'grommet';

type Props = {
	value: string;
	setValue: (value: string) => void;
};

const FunctionType = ({ value, setValue }: Props) => (
	<>
		<Text>Function</Text>
		<RadioButtonGroup
			pad="xsmall"
			name="radio"
			options={[{ label: 'Wave', value: '1' }, { label: 'Explicit', value: '2' }]}
			value={value}
			onChange={(event) => setValue(event.target.value)}
		/>
	</>
);

export default memo(FunctionType);
