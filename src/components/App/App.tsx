import React, { FC, memo } from 'react';
import { Grommet } from 'grommet';
import Canvas from '../Canvas/Canvas';

const App: FC = () => {
	return (
		<Grommet full>
			<Canvas />
		</Grommet>
	);
};

export default memo(App);
