import React, { memo } from 'react';
import { Box, CheckBox } from 'grommet';

type Props = {
	value: boolean;
	handleChange: (value: boolean) => void;
};

const Skybox = ({ handleChange, value }: Props) => (
	<Box align="start" pad="xsmall">
		<CheckBox checked={value} onChange={(event) => handleChange(event.target.checked)} toggle label="Skybox" />
	</Box>
);

export default memo(Skybox);
