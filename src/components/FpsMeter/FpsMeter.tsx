import React from 'react';
import { Meter, Box, Stack, Text } from 'grommet';

type Props = {
	value: number;
};

const FpsMeter = ({ value }: Props) => (
	<Box align="center" pad="xsmall">
		<Stack anchor="center">
			<Meter
				type="circle"
				background="light-2"
				values={[{ value: value, label: `${value} FPS` }]}
				size="xsmall"
				thickness="small"
			/>
			<Box direction="row" align="center" pad={{ bottom: 'xsmall' }}>
				<Text size="xlarge" weight="bold">
					{value}
				</Text>
				<Text size="small">FPS</Text>
			</Box>
		</Stack>
	</Box>
);

export default FpsMeter;
